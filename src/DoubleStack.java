import javax.management.relation.RoleNotFoundException;
import java.util.*;

/** Stack manipulation.
 * @since 1.8
 */
public class DoubleStack {
    private LinkedList<Double> stack = new LinkedList<>();

    public static void main (String[] argum) {

        DoubleStack m = new DoubleStack();
        //m.pop();
      /*m.push(2.4);
      m.push(2.8);
      System.out.println(m);*/
        /*System.out.println("Pop:" + m.pop());*/
      /*m.push(2.9);
      m.push(3.0);
      System.out.println(m);
      m.op("ö");
      System.out.println(m);*/

        Double result = interpret("2. 5. SWAP -");
        System.out.println(result);
    }

    DoubleStack() {
        // TODO!!! Your constructor here!
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        //LinkedList<Double> clone = new LinkedList<>();
        DoubleStack clone = new DoubleStack();

        for (Double aDouble : stack) { //Push all stack element one by one into clone
            clone.push(aDouble);
        }
        return clone;
    }

    public boolean stEmpty() {

        return this.stack.size() == 0; //Return 0 when the stack size is equal to zero
    }

    public void push (double a) {
        this.stack.add(a); //Add element into the last position
    }

    public double pop() {

        /* Checking that stack is not empty */
        if (stEmpty()) {
            throw new RuntimeException("DoubleStack is empty. Can't pop any elements.");
        }

        return this.stack.removeLast(); //Remove and return the last element from the stack
    } // pop

    public void op (String s) { //s operation: +, -, *, /

        /* Checking that there are at least two elements in the stack */
        if (getSize() < 2) {
            throw new RuntimeException("Can't op. There are not enough element in the stack, stackSize:"+getSize()+", operation:" + s);
        }
        double resultDouble;
        double firstDouble = this.pop(); //Get first element from the stack
        double secondDouble = this.pop(); //Get second element from the stack

        switch (s) { //Choose the operation
            case "+":
                resultDouble = secondDouble + firstDouble;
                this.push(resultDouble);
                break;
            case "-":
                resultDouble = secondDouble - firstDouble;
                this.push(resultDouble);
                break;
            case "*":
                resultDouble = secondDouble * firstDouble;
                this.push(resultDouble);
                break;
            case "/":
                if (firstDouble == 0) {
                    throw new RuntimeException("Cant divide with 0");
                }
                resultDouble = secondDouble / firstDouble;
                this.push(resultDouble);
                break;
            default:
                throw new RuntimeException("Can't op. Unknown operation:" + s);
        }
    }

    public double tos() { //Get the last element from the stack but doesn't remove it.

        /* Checking that stack is not empty */
        if (stEmpty()) {
            throw new RuntimeException("DoubleStack is empty. Can't tos a element.");
        }
        return stack.getLast();
    }

    @Override
    public boolean equals (Object o) {
        /* Checking the object m is DoubleStack */
        if (!(o instanceof DoubleStack)) {
            return false;
        }

        DoubleStack other = (DoubleStack) o; //Casting

        if ( other.getSize() != this.getSize()) { //Both objects have to be the same size when they are equals
            return false;
        }

        /* Create clones and take operations using clones */
        DoubleStack otherClone;
        DoubleStack thisClone;
        try {
            otherClone = (DoubleStack) other.clone(); //Casting because clone() returns Object, but I know it is DoubleStack
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return false;
        }
        try {
            thisClone = (DoubleStack) this.clone(); //Casting because clone() returns Object, but I know it is DoubleStack
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return false;
        }
        while (!thisClone.stEmpty()) { //Can check only on object size because both object is the same size
            if( thisClone.pop() != otherClone.pop()) { //Iter and compare both clone elements in the same position one by one.
                return false;
            }
        }

        return true;
    }

    @Override
    public String toString() {

        StringBuilder result = new StringBuilder();
        for (Double aDouble : stack) { //Iter all stack elements

            if (result.toString().equals("")) { //If first element - result string is empty

                result = new StringBuilder(Double.toString(aDouble)); //Add first element
            } else {
                result.append(" ").append(Double.toString(aDouble)); //Add space "" and element
            }
        }
        return result.toString();
    }

    public static double interpret (String pol) {
        DoubleStack stack = new DoubleStack();
        String[] stringList;

        /* Remove multiply spaces, tabs, new lines */
        String convertedPol = pol.replaceAll("\\s+", " "); // "\s": space, tab, new line; "+": one or more;
        convertedPol = convertedPol.trim(); // Remove first and last single spaces

        /* Checking that there are any elements in the string */
        if (convertedPol.length() == 0) {
            throw new RuntimeException("There are not any elements in the input string:'" + pol + "'");
        }

        stringList = convertedPol.split(" "); // Split string to list, delimiter space

        for (String s : stringList) {
            if (isOperator(s)) { //When the string element is operator

                try {
                    stack.op(s); // Take operation using last two operands and push result into stack. Method op().
                } catch (RuntimeException e) {
                    throw new RuntimeException(e + "; Input string:'" + pol + "'");
                }
            } else if (s.equals("SWAP")){

                if( stack.getSize() < 2) {
                    throw new RuntimeException("Too less elements; Input string:'" + pol + "'");
                }

                Double element1 = stack.pop();
                Double element2 = stack.pop();

                stack.push(element1);
                stack.push(element2);

            } else if (s.equals("DUP")) {
                if (stack.stEmpty()) {
                    throw new RuntimeException("Stack is empty");
                }
                stack.push(stack.tos());

            } else if (s.equals("ROT")){

                if( stack.getSize() < 2) {
                    throw new RuntimeException("Too less elements; Input string:'" + pol + "'");
                }

                Double elementC = stack.pop();
                Double elementB = stack.pop();
                Double elementA = stack.pop();

                stack.push(elementB);
                stack.push(elementC);
                stack.push(elementA);

            } else { //When the element is not operator it has to be double.

                try {
                    double element = Double.valueOf(s);
                    stack.push(element); //Push the string element s double value into stack

                } catch (NumberFormatException e) { //When can't convert into double then unknown element
                    throw new RuntimeException("Unknown element:" + s + "; Input string:'" + pol + "'");
                }
            }
        }

        /* Checking that in the stack there are only the result */
        if (stack.getSize() > 1) {
            throw new RuntimeException("There are too many numbers or less operation. Input:'" + pol + "'");
        }

        return stack.pop();
    }

    private int getSize() {
        return this.stack.size();
    }

    private static boolean isOperator(String operator) {

        /* Checking that string is operator: +, -, *, / */
        if (operator.equals("+") || operator.equals("-") || operator.equals("*") || operator.equals("/")) {
            return true;
        }
        return false;
    }

}